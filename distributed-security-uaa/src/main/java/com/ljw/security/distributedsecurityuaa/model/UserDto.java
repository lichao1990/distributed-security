package com.ljw.security.distributedsecurityuaa.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

/**
 * @FileName UserDto
 * @Description TODO   当前登录用户信息
 * @Author ljw
 * @Date 2020/8/31 13:36
 * @Version 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
    private String id;
    private String username;
    private String password;
    private String fullname;
    private String mobile;

    public static final String SESSION_USER_KEY = "_user";
    /*** 用户权限 */
    private Set<String> authorities;
}
