package com.ljw.security.distributedsecurityuaa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class DistributedSecurityUaaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DistributedSecurityUaaApplication.class, args);
	}

}
