package com.ljw.security.distributedsecurityuaa.model;

import lombok.Data;

/**
 * @FileName PermissionDto
 * @Description TODO   权限信息
 * @Author ljw
 * @Date 2020/9/3 7:26
 * @Version 1.0
 */
@Data
public class PermissionDto {
    private String id;
    private String code;
    private String description;
    private String url;
}
