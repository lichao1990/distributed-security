package com.ljw.security.distributedsecurityorder.config;

import com.ljw.security.distributedsecurityorder.handler.CustomAccessDeniedHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;

/**
 * @FileName ResouceServerConfig
 * @Description TODO 资源服务器配置
 * @Author ljw
 * @Date 2020/11/23 22:27
 * @Version 1.0
 */
//@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableResourceServer
@Configuration
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Autowired
    TokenStore tokenStore;

    public static final String RESOURCE_ID = "r1";


    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.resourceId(RESOURCE_ID)
                //.tokenServices(tokenService())//验证普通令牌的服务
                .tokenStore(tokenStore)//使用JWT令牌只需添加这一个
                .stateless(true)
                .accessDeniedHandler(new CustomAccessDeniedHandler());
    }

    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/**").access("#oauth2.hasScope('ROLE_ADMIN')")
                .and()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    //普通令牌解析服务，问题：
    //         当资源服务和授权服务不在一起时,资源服务使用RemoteTokenServices 远程请求授权 服务验证token，如果访问量较大将会影响系统的性能 。
    //解决上边问题：
    //          令牌采用JWT格式即可解决上边的问题，用户认证通过会得到一个JWT令牌，JWT令牌中已经包括了用户相关的信 息，
    //          客户端只需要携带JWT访问资源服务，资源服务根据事先约定的算法自行完成令牌校验，无需每次都请求认证 服务完成授权。
    //1、什么是JWT？
    //      JSON Web Token（JWT）是一个开放的行业标准（RFC 7519），它定义了一种简介的、自包含的协议格式，
    //      用于 在通信双方传递json对象，传递的信息经过数字签名可以被验证和信任。JWT可以使用HMAC算法或使用RSA的公 钥/私钥对来签名，防止被篡改。
    //      JWT令牌由三部分组成，每部分中间使用点（.）分隔，比如：xxxxx.yyyyy.zzzzz
    //      base64UrlEncode(header)：jwt令牌的第一部分。
    //      base64UrlEncode(payload)：jwt令牌的第二部分。
    //      secret：签名所使用的密钥。即SIGNING_KEY
    /*@Bean
    public ResourceServerTokenServices tokenService() {
        //使用远程服务请求授权服务器校验token,必须指定校验token 的url、client_id，client_secret
        RemoteTokenServices service=new RemoteTokenServices();
        service.setCheckTokenEndpointUrl("http://localhost:9001/oauth/check_token");//认证授权服务暴露的端点：/oauth/check_token
        service.setClientId("c1");
        service.setClientSecret("secret");
        return service;
    }*/




}
