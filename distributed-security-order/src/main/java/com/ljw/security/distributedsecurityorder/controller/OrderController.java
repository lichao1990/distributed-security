package com.ljw.security.distributedsecurityorder.controller;

import com.ljw.security.distributedsecurityorder.common.UserDTO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @FileName OrderController
 * @Description TODO
 * @Author ljw
 * @Date 2020/11/23 22:24
 * @Version 1.0
 */
@RestController
@RequestMapping(value = {"/order"})
public class OrderController {
    @GetMapping(value = "/r/r1")
    @PreAuthorize("hasAnyAuthority('p1')")
    public String r1(){
        UserDTO userDTO = (UserDTO) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userDTO.getUsername()+"访问资源1";
    }

    @GetMapping(value = "/r/r2")
    @PreAuthorize("hasAnyAuthority('p2')")
    public String r2(){
        UserDTO userDTO = (UserDTO) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userDTO.getUsername()+"访问资源2";
    }

    @GetMapping(value = "/r3")
    @PreAuthorize("isAuthenticated()")
    public String r3(){
        UserDTO userDTO = (UserDTO) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userDTO.getUsername()+"访问资源3";
    }



}
