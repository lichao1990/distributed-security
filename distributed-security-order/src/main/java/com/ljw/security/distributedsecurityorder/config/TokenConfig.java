package com.ljw.security.distributedsecurityorder.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

/**
 * @FileName TokenConfig
 * @Description TODO 令牌token存储策略：InMemoryTokenStore，JdbcTokenStore，JwtTokenStore，RedisTokenStore
 * @Author ljw
 * @Date 2020/11/22 23:40
 * @Version 1.0
 */
@Configuration
public class TokenConfig {
    /**
     * 普通令牌
     */
   /* @Bean
    public TokenStore tokenStore() {
        //先使用InMemoryTokenStore，生成一个普通的令牌
        return new InMemoryTokenStore();
    }
*/


   /*
   *
   * 下面JWT令牌
   *
   * */

    private String SIGNING_KEY = "uaa123"; //签名key

    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(accessTokenConverter());
    }
    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setSigningKey(SIGNING_KEY); //对称秘钥，资源服务器和授权服务器都使用该秘钥来验证
        return converter;
    }
}
