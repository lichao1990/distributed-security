/*
package com.ljw.security.distributedsecurity.gateway.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;

*/
/**
 * @FileName ResouceServerConfig
 * @Description TODO 资源服务 拦截 配置，即客户端通过网关服务访问资源服务（包括UAA服务）时，网关服务中的security对客户端采取的拦截措施
 * @Author ljw
 * @Date 2020/11/23 22:27
 * @Version 1.0
 *//*


@Configuration
public class ResourceServerConfig{

    public static final String RESOURCE_ID = "r1";
    */
/**统一认证服务(UAA) 资源 拦截
     * 即：客户端（不带token）通过网关服务访问UAA服务时，网关服务中的security对客户端采取的拦截措施
     *//*

    @Configuration
    @EnableResourceServer
    public class UAAServerConfig extends ResourceServerConfigurerAdapter {
        @Autowired
        private TokenStore tokenStore;

        @Override
        public void configure(ResourceServerSecurityConfigurer resources){
            resources.tokenStore(tokenStore)
                    .resourceId(RESOURCE_ID)
                    .stateless(true);
        }

        @Override
        public void configure(HttpSecurity http) throws Exception {
            http.authorizeRequests()
                    .antMatchers("/api/uaa/**").permitAll();//客户端通过网关访问UAA服务时，security不对其拦截
        }
    }

    */
/*** order服务资源拦截，
     * 即：客户端（带着token）通过网关服务访问Order资源服务时，网关服务中的security对客户端采取的拦截措施，目的：校验token，验证客户端接入权限
     * *//*

    @Configuration
    @EnableResourceServer
    public class OrderServerConfig extends ResourceServerConfigurerAdapter {
        @Autowired
        private TokenStore tokenStore;

        @Override
        public void configure(ResourceServerSecurityConfigurer resources){
            resources.tokenStore(tokenStore)
                    .resourceId(RESOURCE_ID)
                    .stateless(true);
        }

        @Override
        public void configure(HttpSecurity http) throws Exception {
            http.authorizeRequests()
                    .antMatchers("/api/order/**").access("#oauth2.hasScope('ROLE_API')");//客户端需要具有ROLE_API权限
        }
    }




}
*/
